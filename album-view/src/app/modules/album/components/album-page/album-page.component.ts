import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpotifyHelperService } from '../../services/spotify-helper.service';

@Component({
  selector: 'app-album-page',
  templateUrl: './album-page.component.html',
  styleUrls: ['./album-page.component.scss']
})
export class AlbumPageComponent implements OnInit {
  @Input() artistId;
  selectedAlbum;
  constructor(private router: Router, private spotifyHelper: SpotifyHelperService) { }

  ngOnInit() {
    this.spotifyHelper.handleUrl();
  }
  onAlbumChange(album) {
    this.selectedAlbum = album;
  }
}
