import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumSelectionComponent } from './album-selection.component';

describe('AlbumSelectionComponent', () => {
  let component: AlbumSelectionComponent;
  let fixture: ComponentFixture<AlbumSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
