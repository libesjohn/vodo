import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-album-selection',
  templateUrl: './album-selection.component.html',
  styleUrls: ['./album-selection.component.scss']
})
export class AlbumSelectionComponent implements OnInit {
  @Input() artistId;
  @Output() onAlbumChange = new EventEmitter();
  albums;
  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getAlbums(this.artistId).subscribe((resp: any) => this.albums = resp.items || []);
  }
  onSelectAlbumChange(album){
    this.onAlbumChange.emit(album);
  }
}
