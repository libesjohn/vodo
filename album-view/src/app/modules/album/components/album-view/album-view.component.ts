import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-album-view',
  templateUrl: './album-view.component.html',
  styleUrls: ['./album-view.component.scss']
})
export class AlbumViewComponent implements OnInit {
  @Input() album;
  isIncludeImg: boolean = true;
  details = [{ title: 'Name', key: 'name' }, { title: 'Release Date', key: 'release_date' }] // can be configured from input...
  constructor() { }

  ngOnInit() {
  }

}
