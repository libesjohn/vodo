import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AlbumPageComponent } from './components/album-page/album-page.component';
import { AlbumSelectionComponent } from './components/album-selection/album-selection.component';
import { AlbumViewComponent } from './components/album-view/album-view.component';

let components=[AlbumPageComponent, AlbumSelectionComponent,AlbumViewComponent];
@NgModule({
  declarations: components,
  exports:components,
  imports: [
    CommonModule,
    FormsModule
  ],

})
export class AlbumModule { }
