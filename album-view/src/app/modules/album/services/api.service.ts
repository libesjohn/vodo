import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpotifyHelperService } from './spotify-helper.service';
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl: string = 'https://api.spotify.com/v1/';
  getAlbumsUrl: string = 'artists/$id/albums?limit=50';
  constructor(private httpClient: HttpClient, private spotifyHelper: SpotifyHelperService) { }

  getAlbums(artistId) {
    let fullUrl = this.baseUrl + this.getAlbumsUrl.replace('$id', artistId);
    return this.httpClient.get(fullUrl, {
      headers: {
        'Authorization': 'Bearer ' + this.spotifyHelper.accessToken
      }
    }).pipe(catchError((resp:any) => { if (resp && resp.status == 401) this.spotifyHelper.login() }));
  }
}
