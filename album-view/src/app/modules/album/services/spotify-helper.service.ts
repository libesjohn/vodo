import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpotifyHelperService {
  spoitfyAuthUrl = "https://accounts.spotify.com/authorize?";
  redirectUrl = "http://localhost:4200";
  clientId = '67460bfc25264d4d8a5f7ca9e1f408e9'
  responseType = 'token'
  accessToken
  constructor() {
    this.accessToken = localStorage.getItem('spotifyToken');
  }
  handleUrl() {
    const url = window.location.href;
    if (url.indexOf('#access_token') == -1) {
      !this.accessToken && this.login();
    }
    else {
      this.accessToken = this.getParameterByName(window.location.hash.replace('#', '?'), 'access_token');
      localStorage.setItem('spotifyToken', this.accessToken);
      window.history.pushState({}, 'Title', '/'); // to clear the url without the access token
    }
  }
  login() {
    window.location.href = this.buildSpotifyLoginUrl();
  }
  buildSpotifyLoginUrl() {
    const url = new URL(this.spoitfyAuthUrl);
    url.searchParams.append('client_id', this.clientId);
    url.searchParams.append('redirect_uri', this.redirectUrl);
    url.searchParams.append('response_type', this.responseType);
    return url.toString();
  }

  getParameterByName(url, name) {
    const match = RegExp('[?&]' + name + '=([^&]*)').exec(url);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
  }
}