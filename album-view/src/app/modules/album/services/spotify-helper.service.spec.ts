import { TestBed } from '@angular/core/testing';

import { SpotifyHelperService } from './spotify-helper.service';

describe('SpotifyHelperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpotifyHelperService = TestBed.get(SpotifyHelperService);
    expect(service).toBeTruthy();
  });
});
